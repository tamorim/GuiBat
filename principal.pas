unit Principal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, process, FileUtil, Forms, Controls, Graphics, Dialogs,
  Menus, ExtCtrls, StdCtrls, ComCtrls, ValEdit, Grids;

type

  { TFrmPrincipal }

  TFrmPrincipal = class(TForm)
    BtnOk: TButton;
    BtnCancel: TButton;
    BtnVoltar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LblNomeBat: TLabel;
    LblCaminhoBat: TLabel;
    LblNomeArq: TLabel;
    LblCaminhoArq: TLabel;
    MainMenu1: TMainMenu;
    MmOpcoes: TMemo;
    MnItmSobre: TMenuItem;
    MnItmAjuda: TMenuItem;
    PgCtrlMain: TPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Process1: TProcess;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ValueLstOpcoes: TValueListEditor;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MnItmAjudaClick(Sender: TObject);
    procedure MnItmSobreClick(Sender: TObject);
    procedure ValueLstOpcoesSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.lfm}

{ TFrmPrincipal }

uses ShellApi;

procedure TFrmPrincipal.MnItmSobreClick(Sender: TObject);
begin
     ShowMessage('GuiBat V0.01' + sLineBreak + 'Tiago Amorim 07.2018');
end;

procedure TFrmPrincipal.ValueLstOpcoesSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
begin
     if acol=0 then CanSelect := false;
end;

procedure TFrmPrincipal.MnItmAjudaClick(Sender: TObject);
begin
     ShowMessage('Programa para criar interface gráfica de chamadas a arquivos .bat.' + sLineBreak +
                 'Chamada indicando o .bat a usar:' + sLineBreak +
                 '    GuiBat.exe "<caminho arquivo>" "<caminho .bat>" "<Descricao_Opcao1[Valor Default]>" "<Descricao_Opcao2[Valor Default]>" ...' + sLineBreak +
                 '  Será criada uma interface para o usuário preencher os valores das opções 1, 2 etc. A descrição de cada opção utilizará o ' +
                 '  string enviado, e virá com o valor padrão descrito. Se não quiser entrar um valor padrão, deixar o espaço em branco. ' +
                 '  Se não precisar fornecer um arquivo para o .bat processar, é preciso entrar as aspas duplas vazias. Ao apertar o botão ' +
                 '  OK será feita uma chamada ao arquivo .bat fornecido:' + sLineBreak +
                 '    "<caminho .bat>" "<caminho arquivo>" <valor opção 1> <valor opção 2> ...' + sLineBreak +
                 'Chamada sem indicar o .bat a usar:' + sLineBreak +
                 '    GuiBat.exe "<caminho arquivo>"' + sLineBreak +
                 '  Na interface gráfico será oferecida uma lista de arquivos .bat previamente carregados para serem utilizados. ' +
                 '  As opções serão lidas no arquivo listabat.txt que existe na mesma pasta do executável GuiBat.exe. Cada linha do ' +
                 '  arquivo terá o seguinte formato: ' + sLineBreak +
                 '    "<caminho .bat>" "<Descricao_Opcao1[Valor Default]>" "<Descricao_Opcao2[Valor Default]>" ...' + sLineBreak +
                 '  Após escolher o arquivo a utilizar, será apresentada a mesma tela de uma chamada indicando o .bat a utilizar.'
                 );
end;

procedure TFrmPrincipal.BtnCancelClick(Sender: TObject);
begin
     Close;
end;

procedure RunShellExecute(const prog,params:string);
begin
  //  ( Handle, nil/'open'/'edit'/'find'/'explore'/'print',   // 'open' isn't always needed
  //      path+prog, params, working folder,
  //        0=hide / 1=SW_SHOWNORMAL / 3=max / 7=min)   // for SW_ constants : uses ... Windows ...
  if ShellExecute(0,'open',PChar(prog),PChar(params),PChar(extractfilepath(prog)),1) >32 then; //success
  // return values 0..32 are errors
end;

procedure TFrmPrincipal.BtnOkClick(Sender: TObject);
var
  i: Integer;
begin
     Process1.Executable := LblCaminhoBat.Caption + LblNomeBat.Caption;
     Process1.Parameters.Add(LblCaminhoArq.Caption + LblNomeArq.Caption);
     for i:=2 to ValueLstOpcoes.RowCount do
         Process1.Parameters.Add(ValueLstOpcoes.Cells[1,i-1]);
     Process1.Execute;
     Close;
     //MmOpcoes.Lines.LoadFromStream(Process1.Output);
end;

function PegaOpcao(opcao: string): string;
var
  i: integer;
begin
     i := Pos('[',opcao);
     if i = 0 then i := Length(opcao)+1;
     if i = 1 then
        PegaOpcao := ''
     else
        PegaOpcao := LeftStr(opcao,i-1);
end;

function PegaDefault(opcao: string): string;
var
  i: integer;
  valor: string;
begin
     i := Pos('[',opcao);
     if i = 0 then
        PegaDefault := ''
     else if RightStr(opcao,1) <> ']' then
          PegaDefault := ''
     else
     begin
         valor := RightStr(opcao, Length(opcao) - i);
         PegaDefault := LeftStr(valor,Length(valor)-1);
     end;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
var
   i: Integer;
   Mensagem, linha, opcao, default: string;
begin
     //Organiza GUI
     LblNomeBat.Caption:='';
     LblCaminhoBat.Caption:='';
     LblNomeArq.Caption:='';
     LblCaminhoArq.Caption:='';

     PgCtrlMain.ShowTabs:=false;
     PgCtrlMain.ActivePage := TabSheet2;
     ValueLstOpcoes.RowCount:=2;
     ValueLstOpcoes.Strings.Clear;

     BtnOk.Enabled:= false;
     BtnVoltar.Enabled:=false;

     for i := 0 to Paramcount do
         MmOpcoes.Lines.Add(ParamStr(i));
     Mensagem := '';
     if Paramcount = 0 then
        Mensagem := 'Entre ao menos uma opção na chamada do programa'
     else
     begin
         if FileExists(ParamStr(1)) then
         begin
              LblNomeBat.Caption := ExtractFileName(ParamStr(2));
              LblCaminhoBat.Caption := ExtractFilePath(ParamStr(2));
         end
         else
         begin
            Mensagem := 'Arquivo não encontrado: ' + ParamStr(1);
            linha := sLineBreak;
         end;
         if Paramcount > 1 then
         begin
              if FileExists(ParamStr(2)) then
              begin
                  LblNomeArq.Caption := ExtractFileName(ParamStr(1));
                  LblCaminhoArq.Caption := ExtractFilePath(ParamStr(1));
              end
              else
              begin
                 Mensagem := Mensagem + linha + 'Arquivo não encontrado: ' + ParamStr(2);
                 linha := sLineBreak;
              end;
              if Paramcount > 2 then ValueLstOpcoes.RowCount:=Paramcount-1;
              for i := 3 to Paramcount do
              begin
                   opcao := PegaOpcao(ParamStr(i));
                   ValueLstOpcoes.Cells[0,i-2] := opcao;
                   if opcao = '' then
                   begin
                      Mensagem := Mensagem + linha + 'Descricao da opção ' + IntToStr(i-2) + ' não encontrada.';
                      linha := sLineBreak;
                   end;
                   default := PegaDefault(ParamStr((i)));
                   ValueLstOpcoes.Cells[1,i-2] := default;
                   if default = '' then
                   begin
                      Mensagem := Mensagem + linha + 'Valor default da opção ' + IntToStr(i-2) + ' não encontrada.';
                      linha := sLineBreak;
                   end;
              end;
         end;
     end;

     if Mensagem <> '' then
     begin
        Mensagem := Mensagem + sLineBreak + sLineBreak + 'Veja "Ajuda" com descrição de como usar o programa.';
        Mensagem := Mensagem + sLineBreak + sLineBreak + 'Valores entrados:';
        for i := 0 to Paramcount do
            Mensagem := Mensagem + sLineBreak + ParamStr(i);
        ShowMessage(Mensagem);
     end
     else
        BtnOk.Enabled:=true;
end;


end.

